import json
import logging
import time

import arivo
from arivo import webhook
from flask import Flask, request, render_template, redirect
from flask_socketio import SocketIO, emit
from requests import HTTPError

from arivo.api import Client

app = Flask(__name__)
app.config["SECRET_KEY"] = "test"
socketio = SocketIO(app)

arivo_api = Client()
arivo.api_token = "nWqxSF8zwFy-jEMbr4FueAEVKoyYLpNQgo2aN0gH3T4"
arivo.api_url = "https://api.digimon.arivo.fun/"

webhook_secret = "fnSLXG3Ictc0U0zbGeNtcJDGK8NMSPL1Hm5Sr-o1974"


def emit_api_result(api_call, res, status_code):
    emit("event", json.dumps({
        "api_call": api_call,
        "timestamp": time.time(),
        "content": res,
        "status_code": status_code,
    }), namespace="/", broadcast=True)


@socketio.on('connect')
def socketio_connect():
    try:
        with open("events.json", "r") as f:
            for line in f.readlines():
                line = line.strip(" \r\n\t")
                if line:
                    emit("event", line)
    except FileNotFoundError:
        pass


@app.route('/webhook', methods=["POST"])
def receive_webhook():
    payload = request.get_data()
    signature = request.headers.get('X-Signature')
    try:
        event = webhook.construct_event(payload, signature, webhook_secret)
    except webhook.InvalidPayload as e:
        logging.error("Invalid webhook payload: %s", payload)
    except webhook.InvalidSignature as e:
        logging.error("Invalid webhook signature")
    else:
        with open("events.json", "a+") as f:
            f.write(json.dumps(event))
            f.write("\n")
        emit("event", json.dumps(event), namespace="/", broadcast=True)
        emit("reload", namespace="/", broadcast=True)
    return "", 201


@app.route("/", methods=["GET"])
def home():
    return render_template("index.html")


@app.route("/payments", methods=["GET"])
def payment_list():
    # query all payments
    payments = arivo_api.Payment.Payment.list(
        payment_method__backend_type="primeo",
        payment_method__source_type="salary",
        ordering="-created"
    )

    # query all customer ids
    customer_ids = set(payment["customer_id"] for payment in payments["results"])
    customers = arivo_api.Backoffice.Customer.list(id__in=",".join(customer_ids))["results"]
    customer_by_id = {x["id"]: x for x in customers}

    # annotate customers to the payments
    for payment in payments["results"]:
        payment["customer"] = customer_by_id.get(payment["customer_id"])

    return render_template("payments.html", payments=payments["results"], count=payments["count"])


@app.route("/payment/<payment_id>/")
def payment_detail(payment_id):
    payment = arivo_api.Payment.Payment.read(id=payment_id)
    customer = arivo_api.Backoffice.Customer.read(id=payment["customer_id"])
    payment_logs = arivo_api.Payment.PaymentLog.list(ordering="created", payment_id=payment["id"])["results"]
    return render_template("payment_detail.html", payment=payment, customer=customer, payment_logs=payment_logs)


@app.route("/payment/<payment_id>/create_log/", methods=["POST"])
def payment_create_log(payment_id):
    payment = arivo_api.Payment.Payment.read(id=payment_id)
    payload_for_reason = {
        "success": {"backend_fee_amount": "1.0", "backend_fee_currency": payment["amount_currency"]},
        "refunded": {"refunded_amount": payment["amount"], "refunded_currency": payment["amount_currency"]},
        "disputed": {"dispute_fee_amount": "2.0", "dispute_fee_currency": payment["amount_currency"]}
    }
    try:
        log = arivo_api.Payment.PaymentLog.create(body={
            "payment_id": payment_id,
            "comment": request.form.get("comment", ""),
            "reason": request.form.get("reason", ""),
            "backend_info": payload_for_reason.get(request.form.get("reason", "")) or {}
        })
        emit_api_result("PaymentLog.create", log, 200)
    except HTTPError as e:
        emit_api_result("PaymentLog.create", e.response.json(), e.response.status_code)
    return redirect(f"/payment/{payment_id}/")


if __name__ == "__main__":
    socketio.run(app, host='0.0.0.0', port=8080, debug=False)
